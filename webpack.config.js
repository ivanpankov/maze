/* eslint-env node */

var path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: path.join(__dirname, 'node_modules'),
                cacheDirectory: true,
                query: {
                    presets: ['es2015', 'stage-0'],
                    plugins: ['transform-decorators-legacy']
                }
            }
        ]
    }
};
