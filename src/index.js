import Maze, {Path} from './CanvasMaze';
import {Dfs, random} from './Maze';


const maze = new Maze(40, 40, 20);
const path = new Path('blue', 'green', maze);

document.getElementById('root').appendChild(maze.canvas);
document.getElementById('root').appendChild(path.canvas);

maze.draw();

const start = maze.m[random(maze.height)][random(maze.width)];
const dfs = new Dfs(maze, start);


const walk = () => {
    let cell = start;

    const step = () => {
        path.draw(cell);

        dfs.next(nextCell => {
            cell = nextCell;
        });

        setTimeout(() => {
            if (cell) {
                requestAnimationFrame(step);
            } else {
                console.log('STOP');
            }
        }, 0);
    };

    step();
};

walk();
