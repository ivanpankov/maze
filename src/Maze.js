export const directions = ['↑', '↓', '←', '→'];
const oppositeDirs = {
    [directions[0]]: directions[1],
    [directions[1]]: directions[0],
    [directions[2]]: directions[3],
    [directions[3]]: directions[2]
};
export const random = to => Math.floor((Math.random() * to));

export default class Maze {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.m = this.matrix(width, height);
        this.generate();
    }

    forEach(cb) {
        const m = this.m;
        const w = this.width;
        const h = this.height;

        for (let y = 0; y < h; y += 1) {
            for (let x = 0; x < w; x += 1) {
                cb.call(this, m[y][x]);
            }
        }
    }

    matrix(width, height) {
        const m = [];

        for (let y = 0; y < height; y += 1) {
            const row = [];

            for (let x = 0; x < width; x += 1) {
                row.push({x, y});
            }

            m.push(row);
        }

        return m;
    }

    getNeighbor = (current, dir) => {
        const cells = this.m;

        switch (dir) {
            case directions[0]:
                return cells[current.y - 1][current.x];

            case directions[1]:
                return cells[current.y + 1][current.x];

            case directions[2]:
                return cells[current.y][current.x - 1];

            case directions[3]:
                return cells[current.y][current.x + 1];

            default:
                return null;
        }
    };

    getDirections = (current) => {
        // available directions
        return [0 < current.y, current.y < this.height - 1, 0 < current.x, current.x < this.width - 1]
        // get non visited directions
            .map((canVisit, index) => canVisit && !this.getNeighbor(current, directions[index]).path ? directions[index] : null)
            .filter(dir => dir);
    };

    setWalls() {
        this.forEach(cell => {
            cell.walls = directions.filter(dir => !cell.path.includes(dir));
        });
    }

    // TODO: find neighbors and remove walls between them
    removeRandomWalls(num) {
        for (let i = 0; i < num; i += 1) {
            const cell = this.m[random(this.height)][random(this.width)];

            if (cell.walls.length > 1) {
                const index = random(cell.walls.length);
                const dir = cell.walls[index];
                cell.walls = [...cell.walls.slice(0, index), ...cell.walls.slice(index + 1)];
                cell.path.push(dir);
            }
        }
    }

    generate() {
        let stack = [];
        const {width, height, getNeighbor, getDirections, m} = this;
        stack.push(m[random(height)][random(width)]);
        stack[0].path = [];

        while (stack.length) {
            const current = stack.pop();
            const dirs = getDirections(current);

            if (dirs.length) {
                stack.push(current);
                const nextDir = dirs[random(dirs.length)];
                current.path.push(nextDir);

                const next = getNeighbor(current, nextDir);
                next.path = next.path || [];
                next.path.push(oppositeDirs[nextDir]);
                stack.push(next);
            }
        }
    }

    getCopy() {
        const newMatrix = this.matrix(this.width, this.height);

        this.forEach(cell => {
            const newCell = newMatrix[cell.y][cell.x];
            newCell.path = [...cell.path];
            newCell.walls = [...cell.walls];
        });

        return newMatrix;
    }
}

export class Dfs {
    constructor(maze, startCell) {
        this.m = maze;
        this.stack = [];
        this.stack.push(startCell || maze.m[0][0]);
        this.clear();
    }

    clear() {
        this.m.forEach(cell => {
            cell.visited = false;
        });
    }

    next(cb) {
        const {stack} = this;

        if (stack.length) {
            const currentCell = stack.pop();
            currentCell.visited = true;

            const neighbors = currentCell.path
                .map(dir => this.m.getNeighbor(currentCell, dir))
                .filter(cell => !cell.visited);

            if (neighbors.length) {
                stack.push(currentCell);
                const nextNode = neighbors[0];
                stack.push(nextNode);
            }

            cb(stack[stack.length - 1]);

        } else {
            cb(null);
        }
    }
}
