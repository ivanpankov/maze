import Maze, {directions} from './Maze';

const drawLine = (ctx, from, to) => {
    ctx.beginPath();
    ctx.moveTo(...from);
    ctx.lineTo(...to);
    ctx.stroke();
};

export const createCanvas = (width, height, cellSize) => {
    const canvas = document.createElement('canvas');

    canvas.width = width * cellSize + 1;
    canvas.height = height * cellSize + 1;
    canvas.style.width = canvas.width + 'px';
    canvas.style.height = canvas.height + 'px';

    return canvas;
};

const getWall = (cell, dir, size) => {
    switch (dir) {
        case directions[0]:
            return [
                cell.position,
                [
                    cell.position[0] + size,
                    cell.position[1]
                ]
            ];

        case directions[1]:
            return [
                [
                    cell.position[0],
                    cell.position[1] + size,
                ],
                [
                    cell.position[0] + size,
                    cell.position[1] + size
                ]
            ];

        case directions[2]:
            return [
                cell.position,
                [
                    cell.position[0],
                    cell.position[1] + size
                ]
            ];

        case directions[3]:
            return [
                [
                    cell.position[0] + size,
                    cell.position[1],
                ],
                [
                    cell.position[0] + size,
                    cell.position[1] + size
                ]
            ];

    }
};

const drawWalls = (cell, ctx, cellSize) => {
    cell.walls.forEach(dir => {
        drawLine(ctx, ...getWall(cell, dir, cellSize));
    });
};


export default class CanvasMaze extends Maze {
    constructor(width, height, cellSize = 30) {
        super(width, height);

        this.cellSize = cellSize;
        this.canvas = createCanvas(width, height, cellSize);
        this.ctx = this.canvas.getContext('2d');

        this.setWalls();
        this.setCellsPosition();
    }

    setCellsPosition() {
        const {cellSize} = this;

        this.forEach(cell => {
            cell.position = [cell.x * cellSize + 0.5, cell.y * cellSize + 0.5];
        });
    }

    drawCell(cell) {
        drawWalls(cell, this.ctx, this.cellSize);
    }

    draw() {
        this.forEach(cell => {
            this.drawCell(cell);
        });
    }
}

export class Path {
    constructor(firstColor, secondColor, maze) {
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.cellSize = maze.cellSize;

        this.canvas = createCanvas(maze.width, maze.height, maze.cellSize);
        this.ctx = this.canvas.getContext('2d');
        this.old = {};

        this.ctx.lineWidth = 4;
        this.ctx.lineJoin="round";
    }

    draw(cell) {
        const x = cell.position[0] + this.cellSize / 2;
        const y = cell.position[1] + this.cellSize / 2;

        this.ctx.beginPath();
        this.ctx.moveTo(this.old.x || x, this.old.y || y);
        this.ctx.lineTo(x, y);
        this.ctx.strokeStyle = !cell.visited ? this.firstColor : this.secondColor;
        this.ctx.closePath();
        this.ctx.stroke();

        this.old.x = x;
        this.old.y = y;
    }

    end() {
        this.ctx.closePath();
    }
}